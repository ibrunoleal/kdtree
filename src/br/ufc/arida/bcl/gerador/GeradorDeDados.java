package br.ufc.arida.bcl.gerador;

import br.ufc.arida.bcl.atributos.Atributo;
import br.ufc.arida.bcl.atributos.Fluxo;
import br.ufc.arida.bcl.atributos.atributos12bits.Atributo12bits;
import br.ufc.arida.bcl.atributos.atributos12bits.AtributoVLanId;
import br.ufc.arida.bcl.atributos.atributos16bits.*;
import br.ufc.arida.bcl.atributos.atributos32bits.Atributo32Bits;
import br.ufc.arida.bcl.atributos.atributos32bits.AtributoIpDestino;
import br.ufc.arida.bcl.atributos.atributos32bits.AtributoIpOrigem;
import br.ufc.arida.bcl.atributos.atributos3bits.Atributo3Bits;
import br.ufc.arida.bcl.atributos.atributos3bits.AtributoVLanPriority;
import br.ufc.arida.bcl.atributos.atributos48bits.Atributo48bits;
import br.ufc.arida.bcl.atributos.atributos48bits.AtributoMacDestino;
import br.ufc.arida.bcl.atributos.atributos48bits.AtributoMacOrigem;
import br.ufc.arida.bcl.atributos.atributos6bits.Atributo6Bits;
import br.ufc.arida.bcl.atributos.atributos6bits.AtributoIPToS;
import br.ufc.arida.bcl.atributos.atributos8bits.Atributo8Bits;
import br.ufc.arida.bcl.atributos.atributos8bits.AtributoIPProtocol;
import br.ufc.arida.bcl.kdtree.No;
import org.apache.commons.math3.random.RandomDataGenerator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by brunoleal on 01/07/16.
 */
public class GeradorDeDados {

    public static final double MEAN_PADRAO_DISTRIBUICAO_NORMAL = 0.0;
    public static final double SIGMA_PADRAO_DISTRIBUICAO_NORMAL = 1.5;

    /*
    Ajustar de acordo com a quantidade de conflitos desejados.
    Quanto maior o valor, menos conflitos serao detectados.
    O valor deve ser definido com base nos valores de mean e sigma da distribuicao. Para os valores padroes
        da classe (vide constantes da classe) o valor 1.3 irá causar em torno de 50% de conflitos.
     */
    public static final double CONSTANTE_DE_VALORES_GENERICOS  = 1.3;

    private List<Long> getNormalSamples3Bits(int sampleSize) {
        return getNormalDataSample(sampleSize, Atributo3Bits.VALOR_MINIMO, Atributo3Bits.VALOR_MAXIMO);
    }

    private List<Long> getNormalSamples6Bits(int sampleSize) {
        return getNormalDataSample(sampleSize, Atributo6Bits.VALOR_MINIMO, Atributo6Bits.VALOR_MAXIMO);
    }

    private List<Long> getNormalSamples8Bits(int sampleSize) {
        return getNormalDataSample(sampleSize, Atributo8Bits.VALOR_MINIMO, Atributo8Bits.VALOR_MAXIMO);
    }

    private List<Long> getNormalSamples12Bits(int sampleSize) {
        return getNormalDataSample(sampleSize, Atributo12bits.VALOR_MINIMO, Atributo12bits.VALOR_MAXIMO);
    }

    private List<Long> getNormalSamples16Bits(int sampleSize) {
        return getNormalDataSample(sampleSize, Atributo16Bits.VALOR_MINIMO, Atributo16Bits.VALOR_MAXIMO);
    }

    private List<Long> getNormalSamples32Bits(int sampleSize) {
        return getNormalDataSample(sampleSize, Atributo32Bits.VALOR_MINIMO, Atributo32Bits.VALOR_MAXIMO);
    }

    private List<Long> getNormalSamples48Bits(int sampleSize) {
        return getNormalDataSample(sampleSize, Atributo48bits.VALOR_MINIMO, Atributo48bits.VALOR_MAXIMO);
    }

    public List<No> getNosNormalizados(int sampleSize) {
        List<Long> listaIngressPort = getNormalSamples16Bits(sampleSize);
        List<Long> listaMacOrigem = getNormalSamples48Bits(sampleSize);
        List<Long> listaMacDestino = getNormalSamples48Bits(sampleSize);
        List<Long> listaEtherType = getNormalSamples16Bits(sampleSize);
        List<Long> listaVlanId = getNormalSamples12Bits(sampleSize);
        List<Long> listaVLanPriority = getNormalSamples3Bits(sampleSize);
        List<Long> listaIPOrigem = getNormalSamples32Bits(sampleSize);
        List<Long> listaIPDestino = getNormalSamples32Bits(sampleSize);
        List<Long> listaIPProtocol = getNormalSamples8Bits(sampleSize);
        List<Long> listaIPToS = getNormalSamples6Bits(sampleSize);
        List<Long> listaPortaOrigem = getNormalSamples16Bits(sampleSize);
        List<Long> listaPortaDestino = getNormalSamples16Bits(sampleSize);

        List<No> listaDeNosNormalizados = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            AtributoIngressPort ingressPort = new AtributoIngressPort(listaIngressPort.get(i));
            AtributoMacOrigem macOrigem = new AtributoMacOrigem(listaMacOrigem.get(i));
            AtributoMacDestino macDestino = new AtributoMacDestino(listaMacDestino.get(i));
            AtributoEtherType etherType = new AtributoEtherType(listaEtherType.get(i));
            AtributoVLanId vLanId = new AtributoVLanId(listaVlanId.get(i));
            AtributoVLanPriority vLanPriority = new AtributoVLanPriority(listaVLanPriority.get(i));
            AtributoIpOrigem ipOrigem = new AtributoIpOrigem(listaIPOrigem.get(i));
            AtributoIpDestino ipDestino = new AtributoIpDestino(listaIPDestino.get(i));
            AtributoIPProtocol ipProtocol = new AtributoIPProtocol(listaIPProtocol.get(i));
            AtributoIPToS ipToS = new AtributoIPToS(listaIPToS.get(i));
            AtributoPortaOrigem portaOrigem = new AtributoPortaOrigem(listaPortaOrigem.get(i));
            AtributoPortaDestino portaDestino = new AtributoPortaDestino(listaPortaDestino.get(i));

            Fluxo fluxo = new Fluxo(ingressPort, macOrigem, macDestino, etherType, vLanId, vLanPriority, ipOrigem,
                    ipDestino, ipProtocol, ipToS, portaOrigem, portaDestino);
            //System.out.println(fluxo);
            No no = new No(fluxo);
            listaDeNosNormalizados.add(no);
        }

        return listaDeNosNormalizados;
    }

    /**
     * Gera uma distribuicao normal com base nos na media (mean/mu) e desvio padrao (sigma) dados.
     * @param media
     *      Media da distribuicao normal.
     * @param desvioPadrao
     *      Desvio padrao da distribuicao.
     * @param sampleSize
     *      Quantidade de amostras a serem geradas.
     * @return
     *      Uma lista com com os valores dos samples gerados.
     */
    private List<Double> gerarDistribuicaoNormal(double media, double desvioPadrao, int sampleSize) {
        RandomDataGenerator dataGenerator = new RandomDataGenerator();
        List<Double> lista = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double v = dataGenerator.nextGaussian(media, desvioPadrao);
            lista.add(v);
        }
        return lista;
    }

    /**
     * Gera uma distribuicao normal com os valores padroes de MEAN e SIGMA.
     *      Obs.: ver constantes da classe para valor padrao de MEAN e SIGMA.
     * @param sampleSize
     *      Quantidade de amostras a serem geradas.
     * @return
     *      Uma lista com com os valores dos samples gerados.
     */
    private List<Double> gerarDistribuicaoNormal(int sampleSize) {
        RandomDataGenerator dataGenerator = new RandomDataGenerator();
        List<Double> lista = new ArrayList<>();
        for (int i = 0; i < sampleSize; i++) {
            double v = dataGenerator.nextGaussian(MEAN_PADRAO_DISTRIBUICAO_NORMAL, SIGMA_PADRAO_DISTRIBUICAO_NORMAL);
            lista.add(v);
        }
        return lista;
    }

    /**
     * Converte uma lista de valores, para valores equivalentes no intervalo (range) dado.
     * @param listaDeValores
     *      Lista de valores da distribuicao original.
     * @param rangeMin
     *      Valor Min do range da lista a ser gerada.
     * @param rangeMax
     *      Valor Max do range da lista a ser gerada.
     * @return
     *      Uma lista transformada para o range dado mantendo a mesma distribuicao.
     */
    private List<Long> transformarDistribuicao(List<Double> listaDeValores, long rangeMin, long rangeMax) {
        List<Long> listaNormalizada = new ArrayList<>();
        double max = Collections.max(listaDeValores);
        double min = Collections.min(listaDeValores);
        double a = (rangeMax - rangeMin) / (max - min);
        double b = rangeMax - a * max;
        for (Double v : listaDeValores) {
            if (v < -CONSTANTE_DE_VALORES_GENERICOS || v > CONSTANTE_DE_VALORES_GENERICOS) {
                long y = Math.round(a * v + b);
                listaNormalizada.add(y);
            } else {
                //o valor mais frequente da distribuicao sera convertido para valor generico.
                listaNormalizada.add(Atributo.VALOR_QUALQUER);
            }
        }
        //System.out.println(listaNormalizada);
        return listaNormalizada;
    }

    /**
     * Gera um data sample seguindo uma distribuicao normal.
     * @param media
     *      Media (mean/mu) da distribuicao normal.
     * @param desvioPadrao
     *      Desvio padrao (sigma) da distribuicao normal.
     * @param sampleSize
     *      Quantidade de amostras a serem geradas.
     * @param rangeMin
     *      Valor minimo de range da amostra.
     * @param rangeMax
     *      Valor maximo de range da amostra.
     * @return
     *      Uma lista com samples de uma distribuicao normal atendendo ao range dado.
     */
    public List<Long> getNormalDataSample(double media, double desvioPadrao, int sampleSize, long rangeMin, long rangeMax) {
        List<Double> distribuicaoNormal = gerarDistribuicaoNormal(media, desvioPadrao, sampleSize);
        return transformarDistribuicao(distribuicaoNormal, rangeMin, rangeMax);
    }

    /**
     * Gera um data sample seguindo uma distribuicao normal.
     *      Obs.: ver constantes da classe para valor padrao de MEAN e SIGMA.
     * @param sampleSize
     *      Quantidade de amostras a serem geradas.
     * @param rangeMin
     *      Valor minimo de range da amostra.
     * @param rangeMax
     *      Valor maximo de range da amostra.
     * @return
     *      Uma lista com samples de uma distribuicao normal atendendo ao range dado.
     */
    public List<Long> getNormalDataSample(int sampleSize, long rangeMin, long rangeMax) {
        List<Double> distribuicaoNormal = gerarDistribuicaoNormal(sampleSize);
        return transformarDistribuicao(distribuicaoNormal, rangeMin, rangeMax);
    }

}
