package br.ufc.arida.bcl.kdtree;

import br.ufc.arida.bcl.atributos.Atributo;
import br.ufc.arida.bcl.atributos.Fluxo;

import java.security.InvalidParameterException;

/**
 * Created by brunoleal on 13/06/16.
 */
public class KdTree {

    /**
     * no raiz da arvore.
     */
    private No raiz;

    public KdTree() {
        this.raiz = null;
    }

    /**
     * Insere um dado no na k-d tree.
     * @param no
     *      no a ser inserido.
     */
    public boolean inserir(No no) {
        if (raiz != null) {
            return inserirRecursivo(raiz, no, 0);
        } else {
            raiz = no;
            return true;
        }
    }

    /**
     * Insercao recursiva de um novo no a partir de um dado no da arvore.
     *      Obs.: se for encontrado um nó igual, a inserção não é realizada.
     * @param noArvore
     *      O no a partir do qual sera feita a insercao na recursao.
     * @param no
     *      O novo no a ser inserido.
     * @param nivel
     *       O nível do noArvore.
     * @return
     *      true se a insercao foi realizada e false c.c.
     */
    private boolean inserirRecursivo(No noArvore, No no, int nivel) throws InvalidParameterException {
        if (no.equals(noArvore)) {
            //se o no igual ja existe na arvore: conflito
            return false;
        }
        if (nivel == Fluxo.DIMENSAO_DO_FLUXO) {
            /* se o nivel da dimensao a ser comparado atingiu o maximo da dimensionalidade
                faz um reset da dimensao (circular) */
            nivel = 0;
        }
        int valorComparado = no.getFluxo().getAtributo(nivel).comparar(noArvore.getFluxo().getAtributo(nivel));
        if (valorComparado != Atributo.COMPARACAO_INVALIDA) {
            if (valorComparado == Atributo.COMPARACAO_MAIOR || valorComparado == Atributo.COMPARACAO_IGUAL) {
                //se maior ou igual
                if (noArvore.getNoFilhoDireita() != null) { // se o proximo no a verificar nao eh nulo
                    //se existe filho da direita tentar inserir recursivamente a partir do no filho da direita
                    nivel++;
                    return inserirRecursivo(noArvore.getNoFilhoDireita(), no, nivel);
                } else {
                    //nao existem mais nos a comparar e deve ser inserido aqui (filho da direita)
                    noArvore.setNoFilhoDireita(no);
                    return true;
                }
            } else {
                //se menor
                if (noArvore.getNoFilhoEsquerda() != null) {
                    //se existe filho na esquerda tentar inserir recursivamente a partir do no filho da esquerda
                    nivel++;
                    return inserirRecursivo(noArvore.getNoFilhoEsquerda(), no, nivel);
                } else {
                    //nao existem mais nos a comparar e deve ser inserido aqui (filho da esquerda)
                    noArvore.setNoFilhoEsquerda(no);
                    return true;
                }
            }
        } else {
            throw new InvalidParameterException("Erro de comparacao: atributos invalidos para comparacao.");
        }
    }

    /**
     * Pesquisa um dado no na K-d Tree
     * @param no
     *      br.ufc.arida.bcl.kdtree.No a ser pesquisado.
     */
    public No pesquisar(No no) {
       return pesquisarRecursivo(no, raiz, 0);
    }

    /**
     * Pesquisa recursiva de um no a partir de um dado no da arvore.
     * @param no
     *      O no a ser pesquisado.
     * @param noArvore
     *      O no a partir do qual sera feita a busca na recursao.
     * @param nivel
     *      O nivel do noArvore.
     * @return
     *      O no caso exista, e null c.c.
     */
    private No pesquisarRecursivo(No no, No noArvore, int nivel) {
        if (noArvore != null) {
            if (no.equals(noArvore)) {
                return noArvore;
            } else {
                int valorComparado = no.getFluxo().getAtributo(nivel).comparar(noArvore.getFluxo().getAtributo(nivel));
                if (valorComparado != Atributo.COMPARACAO_INVALIDA) {
                    if (valorComparado == Atributo.COMPARACAO_MAIOR || valorComparado == Atributo.COMPARACAO_IGUAL) { //se maior ou igual)
                        noArvore = noArvore.getNoFilhoDireita();
                    } else {
                        noArvore = noArvore.getNoFilhoEsquerda();
                    }
                    return pesquisarRecursivo(no, noArvore, nivel++);
                } else {
                    return null;
                }
            }
        } else {
            return null;
        }
    }

    public void imprimir() {
        imprimirRecursivo(raiz);
    }

    private void imprimirRecursivo(No no){
        if (no != null) {
            String impressao = "" + no;
            impressao += "\n    Filho Esquerda -> " + no.getNoFilhoEsquerda();
            impressao += "\n    Filho Direita -> " + no.getNoFilhoDireita();
            System.out.println(impressao);

            //Chamada recursiva.
            if (no.getNoFilhoEsquerda() != null) {
                imprimirRecursivo(no.getNoFilhoEsquerda());
            }
            if (no.getNoFilhoDireita() != null) {
                imprimirRecursivo(no.getNoFilhoDireita());
            }
        }
    }

}
