package br.ufc.arida.bcl.kdtree;

import br.ufc.arida.bcl.atributos.Fluxo;

import java.util.Objects;

/**
 * Created by brunoleal on 13/06/16.
 */
public class No {

    private Fluxo fluxo;

    private No noFilhoEsquerda;

    private No noFilhoDireita;


    public No(Fluxo fluxo) {
        this.fluxo = fluxo;
        this.noFilhoEsquerda = null;
        this.noFilhoDireita = null;
    }

    public No getNoFilhoEsquerda() {
        return noFilhoEsquerda;
    }

    public void setNoFilhoEsquerda(No noFilhoEsquerda) {
        this.noFilhoEsquerda = noFilhoEsquerda;
    }

    public No getNoFilhoDireita() {
        return noFilhoDireita;
    }

    public void setNoFilhoDireita(No noFilhoDireita) {
        this.noFilhoDireita = noFilhoDireita;
    }

    public Fluxo getFluxo() {
        return fluxo;
    }

    @Override
    public String toString() {
        return "No{" +
                "fluxo=" + fluxo +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        No no = (No) o;
        return fluxo.equals(no.getFluxo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(fluxo);
    }
}
