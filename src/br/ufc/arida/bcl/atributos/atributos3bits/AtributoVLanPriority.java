package br.ufc.arida.bcl.atributos.atributos3bits;

import br.ufc.arida.bcl.atributos.Atributo;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoVLanPriority extends Atributo3Bits {

    public AtributoVLanPriority(long valor) {
        super(valor);
    }

    public AtributoVLanPriority() {
        super();
    }

    public String toString() {
        return "AtributoVLanPriority(" + super.toString() + ')';
    }
}
