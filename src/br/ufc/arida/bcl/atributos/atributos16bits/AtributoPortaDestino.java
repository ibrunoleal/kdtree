package br.ufc.arida.bcl.atributos.atributos16bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoPortaDestino extends Atributo16Bits {

    public AtributoPortaDestino(long valor) {
        super(valor);
    }

    public AtributoPortaDestino() {
        super();
    }

    public String toString() {
        return "PortaDestino(" + super.toString() + ')';
    }
}
