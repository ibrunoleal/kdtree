package br.ufc.arida.bcl.atributos.atributos16bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoEtherType extends Atributo16Bits {

    public AtributoEtherType(long valor) {
        super(valor);
    }

    public AtributoEtherType() {
        super();
    }

    public String toString() {
        return "EtherType(" + super.toString() + ')';
    }

}
