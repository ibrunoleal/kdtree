package br.ufc.arida.bcl.atributos.atributos16bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoPortaOrigem extends Atributo16Bits {

    public AtributoPortaOrigem(long valor) {
        super(valor);
    }

    public AtributoPortaOrigem() {
        super();
    }

    public String toString() {
        return "PortaOrigem(" + super.toString() + ')';
    }
}
