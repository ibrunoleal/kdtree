package br.ufc.arida.bcl.atributos.atributos16bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoIngressPort extends Atributo16Bits {

    public AtributoIngressPort(long valor) {
        super(valor);
    }

    public AtributoIngressPort() {
        super();
    }

    @Override
    public String toString() {
        return "IngressPort(" + super.toString() + ')';
    }
}
