package br.ufc.arida.bcl.atributos.atributos16bits;

import br.ufc.arida.bcl.atributos.Atributo;

import java.util.Objects;

/**
 * Created by brunoleal on 14/06/16.
 */
public abstract class Atributo16Bits extends Atributo {

    public static final long VALOR_MINIMO = 0;
    public static final long VALOR_MAXIMO = 65535;

    public Atributo16Bits(long valor) {
        super(valor);
    }

    public Atributo16Bits() {
        super();
    }

    /**
     * Define o valor do atributo de 16 bits.
     * So aceita valores de 0 a 65535. Para outros valores define como valor invalido.
     *      obs.: valor invalido utiliza constante da classe Atributo.
     * @param valor
     *      Valor a ser definido para o atributo.
     */
    @Override
    public void setValor(long valor) {
        if ((valor >= VALOR_MINIMO && valor <= VALOR_MAXIMO) || valor == Atributo.VALOR_QUALQUER) {
            super.setValor(valor);
        } else {
            setValor(Atributo.VALOR_INVALIDO);
        }
    }

}
