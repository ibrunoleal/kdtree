package br.ufc.arida.bcl.atributos;

/**
 * Created by brunoleal on 14/06/16.
 */
public abstract class Atributo {

    public static final int COMPARACAO_INVALIDA = -99;
    public static final int COMPARACAO_MAIOR = 1;
    public static final int COMPARACAO_IGUAL = 0;
    public static final int COMPARACAO_MENOR = -1;

    public static final long VALOR_QUALQUER = -100;
    public static final long VALOR_INVALIDO = -999;

    private long valor;

    /**
     * Inicia o Atributo com o valor dado.
     *      obs.: o valor definido utilizando metodo setValor.
     * @param valor
     *      Valor a ser definido para o atributo.
     */
    public Atributo(long valor) {
        setValor(valor);
    }

    /**
     * Inicia o Atributo com valor generico (valor qualquer).
     *      obs.: utiliza a constante da classe.
     */
    public Atributo() {
        this.valor = VALOR_QUALQUER;
    }

    public void setValor(long valor) {
        this.valor = valor;
    }

    /**
     * Recupera o valor do atributo.
     * @return
     *      o valor do atributo.
     */
    public long getValor() {
        return valor;
    }

    /**
     * Compara se os atributos sao do mesmo tipo e possuem os mesmos valores.
     * @param outroAtributo
     *      O outro atributo a ser comparado com o objeto (this).
     * @return
     *      COMPARACAO_INVALIDA se os atributos comparados nao sao do mesmo tipo ou se o outro e nulo;
     *      COMPARACAO_IGUAL pelo menos um dos atributos comparados tem VALOR_QUALQUER ou se eles têm valores iguais;
     *      COMPARACAO_MAIOR se o objeto(this) tem valor maior que o outro;
     *      COMPARACAO_MENOR caso contrario (se o objeto(this) tem valor menor que o outro).
     *
     *      Obs.: utiliza as constantes da classe.
     */
    public int comparar(Atributo outroAtributo) {
        if (outroAtributo == null) {
            return COMPARACAO_INVALIDA;
        }
        if (outroAtributo.getClass() != getClass()) {
            return COMPARACAO_INVALIDA;
        }

        if (getValor() == Atributo.VALOR_QUALQUER || outroAtributo.getValor() == Atributo.VALOR_QUALQUER) {
            return COMPARACAO_IGUAL;
        }
        if (getValor() == outroAtributo.getValor()) {
            return COMPARACAO_IGUAL;
        } else {
            if (getValor() > outroAtributo.getValor()) {
                return COMPARACAO_MAIOR;
            } else {
                return COMPARACAO_MENOR;
            }
        }
    }

    @Override
    public String toString() {
        if (valor == Atributo.VALOR_QUALQUER) {
            return "---";
        } else {
            return "" + valor;
        }
    }
}
