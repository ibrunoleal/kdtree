package br.ufc.arida.bcl.atributos.atributos32bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoIpDestino extends Atributo32Bits {

    public AtributoIpDestino(long valor) {
        super(valor);
    }

    public AtributoIpDestino() {
        super();
    }

    public String toString() {
        return "IPDestino(" + super.toString() + ')';
    }

}
