package br.ufc.arida.bcl.atributos.atributos32bits;

import br.ufc.arida.bcl.atributos.Atributo;

/**
 * Created by brunoleal on 30/06/16.
 */
public abstract class Atributo32Bits extends Atributo {

    public static final long VALOR_MINIMO = 0;
    public static final long VALOR_MAXIMO = 4294967295l;

    public Atributo32Bits(long valor) {
        super(valor);
    }

    public Atributo32Bits() {
        super();
    }

    /**
     * Define o valor do IP.
     * So aceita valores de 0 a 4294967295. Para outros valores define como valor invalido.
     *      obs.: valor invalido utiliza constante da classe Atributo.
     * @param valor
     *      Valor a ser definido para o atributo.
     */
    @Override
    public void setValor(long valor) {
        if ((valor >= VALOR_MINIMO && valor <= VALOR_MAXIMO) || valor == Atributo.VALOR_QUALQUER) {
            super.setValor(valor);
        } else {
            setValor(Atributo.VALOR_INVALIDO);
        }
    }
}
