package br.ufc.arida.bcl.atributos.atributos32bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoIpOrigem extends Atributo32Bits {

    public AtributoIpOrigem(long valor) {
        super(valor);
    }

    public AtributoIpOrigem() {
        super();
    }

    public String toString() {
        return "IPOrigem(" + super.toString() + ')';
    }
}
