package br.ufc.arida.bcl.atributos.atributos6bits;

import br.ufc.arida.bcl.atributos.Atributo;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoIPToS extends Atributo6Bits {

    public AtributoIPToS(long valor) {
        super(valor);
    }

    public AtributoIPToS() {
        super();
    }



    public String toString() {
        return "IPToS(" + super.toString() + ')';
    }
}
