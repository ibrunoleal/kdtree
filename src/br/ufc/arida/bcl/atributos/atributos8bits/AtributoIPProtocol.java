package br.ufc.arida.bcl.atributos.atributos8bits;

import br.ufc.arida.bcl.atributos.Atributo;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoIPProtocol extends Atributo8Bits {

    public AtributoIPProtocol(long valor) {
        super(valor);
    }

    public AtributoIPProtocol() {
        super();
    }

    public String toString() {
        return "IPProtocol(" + super.toString() + ')';
    }
}
