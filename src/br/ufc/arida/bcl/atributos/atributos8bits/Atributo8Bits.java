package br.ufc.arida.bcl.atributos.atributos8bits;

import br.ufc.arida.bcl.atributos.Atributo;

/**
 * Created by brunoleal on 01/07/16.
 */
public abstract class Atributo8Bits extends Atributo {

    public static final long VALOR_MINIMO = 0;
    public static final long VALOR_MAXIMO = 255;

    public Atributo8Bits(long valor) {
        super(valor);
    }

    public Atributo8Bits() {
        super();
    }

    /**
     * Define o valor do atributo de 8 bits.
     * So aceita valores de 0 a 255. Para outros valores define como valor invalido.
     *      obs.: valor invalido utiliza constante da classe Atributo.
     * @param valor
     *      Valor a ser definido para o atributo.
     */
    @Override
    public void setValor(long valor) {
        if ((valor >= VALOR_MINIMO && valor <= VALOR_MAXIMO) || valor == Atributo.VALOR_QUALQUER) {
            super.setValor(valor);
        } else {
            setValor(Atributo.VALOR_INVALIDO);
        }
    }
}
