package br.ufc.arida.bcl.atributos.atributos48bits;

import br.ufc.arida.bcl.atributos.Atributo;

import java.util.Objects;

/**
 * Created by brunoleal on 22/06/16.
 */
public abstract class Atributo48bits extends Atributo {

    public static final long VALOR_MINIMO = 0;
    public static final long VALOR_MAXIMO = 218474976710655l;

    public Atributo48bits(long valor) {
        super(valor);
    }

    public Atributo48bits() {
        super();
    }

    /**
     * Define o valor do atributo de 48 bits.
     * So aceita valores de 0 a 218474976710655. Para outros valores define como valor invalido.
     *      obs.: valor invalido utiliza constante da classe Atributo.
     * @param valor
     *      Valor a ser definido para o atributo.
     */
    @Override
    public void setValor(long valor) {
        if ((valor >= VALOR_MINIMO && valor <= VALOR_MAXIMO) || valor == Atributo.VALOR_QUALQUER) {
            super.setValor(valor);
        } else {
            setValor(Atributo.VALOR_INVALIDO);
        }
    }

}
