package br.ufc.arida.bcl.atributos.atributos48bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoMacDestino extends Atributo48bits {

    public AtributoMacDestino(long valor) {
        super(valor);
    }

    public AtributoMacDestino() {
        super();
    }

    public String toString() {
        return "MACDestino(" + super.toString() + ')';
    }
}
