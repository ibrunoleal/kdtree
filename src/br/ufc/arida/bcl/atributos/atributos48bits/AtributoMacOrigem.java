package br.ufc.arida.bcl.atributos.atributos48bits;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoMacOrigem extends Atributo48bits {

    public AtributoMacOrigem(long valor) {
        super(valor);
    }

    public AtributoMacOrigem() {
        super();
    }

    public String toString() {
        return "MACOrigem(" + super.toString() + ')';
    }
}
