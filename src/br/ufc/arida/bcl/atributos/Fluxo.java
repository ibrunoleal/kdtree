package br.ufc.arida.bcl.atributos;

import br.ufc.arida.bcl.atributos.atributos12bits.AtributoVLanId;
import br.ufc.arida.bcl.atributos.atributos16bits.AtributoEtherType;
import br.ufc.arida.bcl.atributos.atributos16bits.AtributoIngressPort;
import br.ufc.arida.bcl.atributos.atributos16bits.AtributoPortaDestino;
import br.ufc.arida.bcl.atributos.atributos16bits.AtributoPortaOrigem;
import br.ufc.arida.bcl.atributos.atributos32bits.AtributoIpDestino;
import br.ufc.arida.bcl.atributos.atributos32bits.AtributoIpOrigem;
import br.ufc.arida.bcl.atributos.atributos3bits.AtributoVLanPriority;
import br.ufc.arida.bcl.atributos.atributos48bits.AtributoMacDestino;
import br.ufc.arida.bcl.atributos.atributos48bits.AtributoMacOrigem;
import br.ufc.arida.bcl.atributos.atributos6bits.AtributoIPToS;
import br.ufc.arida.bcl.atributos.atributos8bits.AtributoIPProtocol;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by brunoleal on 22/06/16.
 */
public class Fluxo {

    public static final int DIMENSAO_DO_FLUXO = 12;

    private List<Atributo> listaDeAtributos;

    public Fluxo() {
        this.listaDeAtributos = new ArrayList<>(12);
        definirFluxoGenerico();
    }


    public Fluxo(
            AtributoIngressPort ingressPort,
            AtributoMacOrigem macOrigem,
            AtributoMacDestino macDestino,
            AtributoEtherType etherType,
            AtributoVLanId vLanId,
            AtributoVLanPriority vLanPriority,
            AtributoIpOrigem ipOrigem,
            AtributoIpDestino ipDestino,
            AtributoIPProtocol ipProtocol,
            AtributoIPToS ipToS,
            AtributoPortaOrigem portaOrigem,
            AtributoPortaDestino portaDestino)
    {
        this.listaDeAtributos = new ArrayList<>(12);
        definirFluxoGenerico();
        listaDeAtributos.set(0, ingressPort);
        listaDeAtributos.set(1, macOrigem);
        listaDeAtributos.set(2, macDestino);
        listaDeAtributos.set(3, etherType);
        listaDeAtributos.set(4, vLanId);
        listaDeAtributos.set(5, vLanPriority);
        listaDeAtributos.set(6, ipOrigem);
        listaDeAtributos.set(7, ipDestino);
        listaDeAtributos.set(8, ipProtocol);
        listaDeAtributos.set(9, ipToS);
        listaDeAtributos.set(10, portaOrigem);
        listaDeAtributos.set(11, portaDestino);
    }

    private void definirFluxoGenerico() {
        AtributoIngressPort ingressPort = new AtributoIngressPort();
        AtributoMacOrigem macOrigem = new AtributoMacOrigem();
        AtributoMacDestino macDestino = new AtributoMacDestino();
        AtributoEtherType etherType = new AtributoEtherType();
        AtributoVLanId vLanId = new AtributoVLanId();
        AtributoVLanPriority vLanPriority = new AtributoVLanPriority();
        AtributoIpOrigem ipOrigem = new AtributoIpOrigem();
        AtributoIpDestino ipDestino = new AtributoIpDestino();
        AtributoIPProtocol ipProtocol = new AtributoIPProtocol();
        AtributoIPToS ipToS = new AtributoIPToS();
        AtributoPortaOrigem portaOrigem = new AtributoPortaOrigem();
        AtributoPortaDestino portaDestino = new AtributoPortaDestino();

        listaDeAtributos.add(0, ingressPort);
        listaDeAtributos.add(1, macOrigem);
        listaDeAtributos.add(2, macDestino);
        listaDeAtributos.add(3, etherType);
        listaDeAtributos.add(4, vLanId);
        listaDeAtributos.add(5, vLanPriority);
        listaDeAtributos.add(6, ipOrigem);
        listaDeAtributos.add(7, ipDestino);
        listaDeAtributos.add(8, ipProtocol);
        listaDeAtributos.add(9, ipToS);
        listaDeAtributos.add(10, portaOrigem);
        listaDeAtributos.add(11, portaDestino);
    }

    /**
     * Retorna o atributo da dimensao especificada.
     * @param dimensao
     *      Nível da dimensao do atributo.
     *      Obs.: o primeiro nível é 0(zero).
     * @return
     *      O atributo da dimensao, caso a dimensao exista e null c.c.
     */
    public Atributo getAtributo(int dimensao) {
        if (dimensao < DIMENSAO_DO_FLUXO) {
            return listaDeAtributos.get(dimensao);
        }
        return null;
    }

    public void inserirAtributo(Atributo atributo) {
        int posicao = -1;

        if (atributo instanceof AtributoIngressPort) {
            posicao = 0;
        } else if (atributo instanceof AtributoMacOrigem){
            posicao = 1;
        } else if (atributo instanceof AtributoMacDestino) {
            posicao = 2;
        } else if (atributo instanceof AtributoEtherType) {
            posicao = 3;
        } else if (atributo instanceof AtributoVLanId) {
            posicao = 4;
        } else if (atributo instanceof AtributoVLanPriority) {
            posicao = 5;
        } else if (atributo instanceof AtributoIpOrigem) {
            posicao = 6;
        } else if (atributo instanceof AtributoIpDestino) {
            posicao = 7;
        } else if (atributo instanceof AtributoIPProtocol) {
            posicao = 8;
        } else if (atributo instanceof AtributoIPToS) {
            posicao = 9;
        } else if (atributo instanceof AtributoPortaOrigem) {
            posicao = 10;
        } else if (atributo instanceof AtributoPortaDestino) {
            posicao = 11;
        }

        if (posicao != -1) {
            listaDeAtributos.set(posicao, atributo);
        }
    }

    @Override
    public String toString() {
        return "" + listaDeAtributos +
                "";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fluxo outroFluxo = (Fluxo) o;
        for (int d = 0; d < listaDeAtributos.size(); d++) {
            if (getAtributo(d).comparar(outroFluxo.getAtributo(d)) != Atributo.COMPARACAO_IGUAL) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        return Objects.hash(listaDeAtributos);
    }
}
