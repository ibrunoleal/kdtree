package br.ufc.arida.bcl.atributos.atributos12bits;

import br.ufc.arida.bcl.atributos.Atributo;

/**
 * Created by brunoleal on 01/07/16.
 */
public abstract class Atributo12bits extends Atributo {

    public static final long VALOR_MINIMO = 0;
    public static final long VALOR_MAXIMO = 4095;

    public Atributo12bits(long valor) {
        super(valor);
    }

    public Atributo12bits() {
        super();
    }

    /**
     * Define o valor do atributo de 12 bits.
     * So aceita valores de 0 a 4095. Para outros valores define como valor invalido.
     *      obs.: valor invalido utiliza constante da classe Atributo.
     * @param valor
     *      Valor a ser definido para o atributo.
     */
    @Override
    public void setValor(long valor) {
        if ((valor >= VALOR_MINIMO && valor <= VALOR_MAXIMO) || valor == Atributo.VALOR_QUALQUER) {
            super.setValor(valor);
        } else {
            setValor(Atributo.VALOR_INVALIDO);
        }
    }
}
