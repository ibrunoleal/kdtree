package br.ufc.arida.bcl.atributos.atributos12bits;

import br.ufc.arida.bcl.atributos.Atributo;

/**
 * Created by brunoleal on 30/06/16.
 */
public class AtributoVLanId extends Atributo12bits {

    public AtributoVLanId(long valor) {
        super(valor);
    }

    public AtributoVLanId() {
        super();
    }

    public String toString() {
        return "VLanId(" + super.toString() + ')';
    }
}
