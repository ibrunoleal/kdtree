package br.ufc.arida.bcl;

import br.ufc.arida.bcl.gerador.GeradorDeDados;
import br.ufc.arida.bcl.kdtree.KdTree;
import br.ufc.arida.bcl.kdtree.No;

import java.util.List;

/**
 * Created by brunoleal on 15/06/16.
 */
public class Main {

    public static void main(String[] args) {

        KdTree kdTree = new KdTree();

        int NUMERO_DE_NOS = 10;
        GeradorDeDados geradorDeNos = new GeradorDeDados();
        List<No> listaDeNosRandomicos = geradorDeNos.getNosNormalizados(NUMERO_DE_NOS);

        int cont = 0;
        System.out.println("***********************************************");
        long tempoInicial = System.currentTimeMillis();
        for (No no: listaDeNosRandomicos) {
            //System.out.println(no);
            if (!kdTree.inserir(no)){
                cont++;
            }
        }
        long tempoFinal = System.currentTimeMillis();
        System.out.println("tempo de execucao = " + (tempoFinal - tempoInicial) + " milissegundos");
        System.out.println("numero de conflitos = " + cont);

        //visualizacao da arvore em terminal
        //kdTree.imprimir();

    }

}
